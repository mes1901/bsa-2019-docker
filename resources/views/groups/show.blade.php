@extends('layouts.app')
@section('content')
    <div class="container col-sm-3">
        <div class="card-body">
            <div class="form-group">
                <div class="row">
                    <a href="/"><- BACK</a>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <span>Group: </span>
                    </div>
                    <div id="group_name" class="col-sm-1">
                        <span>{{$group->name}}</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <span>Team: </span>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" name="name" id="team_name" maxlength="16"
                               class="form-control form-control-sm">
                    </div>
                    <div class="col-sm-4">
                        <button type="submit" name="add_team" class="btn btn-success btn-sm btn-block">Add</button>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <span>Teams: </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="team_list" class="table table-borderless table-sm table-responsive">
                            @if(count($teams) > 0)
                                @foreach($teams as $team)
                                    <tr>
                                        <td>
                                            {{$team->name}}
                                        </td>
                                        <td>
                                            @if(!$team->active)
                                                <button name="delete_team" value="{{$team->name}}"
                                                        class="btn btn-danger btn-sm">
                                                    Delete
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-sm-8">
                        <span>Matches: </span>
                    </div>
                    <div class="col-sm-4">
                        <button type="submit" name="generate_matches" class="btn btn-success btn-sm btn-block">
                            Generate
                        </button>
                    </div>
                    <div class="col-sm-12">
                        <table id="matches_list" class="table table-borderless table-sm table-responsive">
                            @if(count($data_matches) > 0)
                                @foreach($data_matches as $match)
                                    <tr>
                                        <td>
                                            {{$match->first_team_name}}
                                        </td>
                                        <td>
                                            <input type="text" name="score" id="first_team_score" maxlength="3" size="1"
                                                   class="form-control form-control-sm text-center"
                                                   value="{{$match->first_team_score}}">
                                        </td>
                                        <td>
                                            <span>:</span>
                                        </td>
                                        <td>
                                            <input type="text" name="score" id="second_team_score" maxlength="3"
                                                   size="1"
                                                   class="form-control form-control-sm text-center"
                                                   value="{{$match->second_team_score}}">
                                        </td>
                                        <td>
                                            {{$match->second_team_name}}
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ URL::asset('js/teams.js') }}"></script>
@endsection
