<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'GroupsController@index');
Route::get('/groups/{group}', 'GroupsController@show');

Route::post('create_new_group', 'GroupsController@create_new_group');
Route::delete('delete_group', 'GroupsController@delete_group');

Route::post('/create_new_team', 'TeamsController@create_team');
Route::delete('/delete_team', 'TeamsController@delete_team');

Route::post('/add_matches', 'MatchesController@add_matches');
Route::post('/set_active_teams', 'MatchesController@active_teams');
Route::post('/set_inactive_teams', 'MatchesController@inactive_teams');

