<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matches extends Model
{
    public $timestamps = false;
    protected $table = 'matches';

    public function get_matches($id)
    {
        return static::where('group_id', $id)->get();
    }

    public function create($first_team, $second_team, $group_id)
    {
        $new_match = new Matches();
        $new_match->first_team_id = (new Teams())->get_team_id($first_team)->id;
        $new_match->second_team_id = (new Teams())->get_team_id($second_team)->id;
        $new_match->group_id = $group_id;
        $new_match->save();
    }
}
