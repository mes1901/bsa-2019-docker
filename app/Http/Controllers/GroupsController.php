<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Illuminate\Support\Arr;
use PhpParser\Node\Expr\Array_;

class GroupsController extends Controller
{

    public function index()
    {
        $groups = App\Groups::all();
        return view('groups.index', compact('groups'));
    }

    public function show($name)
    {
        $group = App\Groups::where('name', '=', $name)->first();
        $teams = (new App\Teams())->get_teams($group->id);
        $matches = (new App\Matches())->get_matches($group->id);
        $data_matches = [];
        foreach ($matches as $match){
            $data = new App\Match();
            $first_team_name = $data->get_team_name($match->first_team_id);
            $second_team_name = $data->get_team_name($match->second_team_id);
            $group_name = $data->get_group_name($match->group_id);
            $data->init($first_team_name->name, $second_team_name->name, $match->first_team_score, $match->second_team_score, $group_name->name);
            array_push($data_matches, $data);
        }
        return view('groups.show', compact(['group', 'teams', 'data_matches']));
    }

    public function create_new_group(Request $request)
    {
        $group_name = json_decode($request->getContent(), true);
        $group = new App\Groups();
        $group->name = $group_name;
        $group->save();
        $send = json_encode(['group_added']);
        return $send;
    }

    public function delete_group(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $group = App\Groups::where('name', $data['name'])->first();
        $group->delete();
        $send = json_encode(['group_deleted', $data['id']]);
        return $send;

    }

    public function insert()
    {

    }
}
