<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class TeamsController extends Controller
{
    public function create_team(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $new_team = new App\Teams();
        $new_team->create($data['group_name'], $data['team_name']);
        $send = json_encode(['team_created', $data['team_name']]);
        return $send;
    }

    public function delete_group(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $group = App\Groups::where('name', $data['name'])->first();
        $group->delete();
        $send = json_encode(['group_deleted', $data['id']]);
        return $send;

    }

    public function delete_team(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $group_id = (new App\Groups())->get_group_id($data['group_name'])->id;
        $team = App\Teams::where([
            ['name', $data['team_name']],
            ['group_id', $group_id]
        ])->first();
        $team->delete();

        $matches = (new App\Matches())->get_matches($group_id);
        $data_matches = [];
        if (count($matches) > 0) {
            foreach ($matches as $match) {
                $data_match = new App\Match();
                $first_team_name = $data_match->get_team_name($match->first_team_id);
                $second_team_name = $data_match->get_team_name($match->second_team_id);
                $group_name = $data['group_name'];
                $data_match->init($first_team_name->name, $second_team_name->name, $match->first_team_score, $match->second_team_score, $group_name);
                array_push($data_matches, $data_match);
            }
        }

        $send = json_encode(['team_deleted', $data['id'], $data_matches]);
        return $send;
    }
}
